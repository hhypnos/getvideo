# getVideo

Get url of video from web page(imovies) (chrome extension)

# Installation

<ul>
<li> Clone repository</li>
<li> Unzip it</li>
<li> Open extensions setting and turn on developer mode (click three dots in the right corner of google chrome , More tools > Extensions)</li>
<li> Click "load unpacked" and choose clonned repository</li>
</ul>

# Usage

<ul>
<li> Go to any movie WebPage</li>
<li> Choose movie</li>
<li> Choose language</li>
<li> Click extension</li>
<li> Click scan</li>
<li> Go to the link</li>
</ul>